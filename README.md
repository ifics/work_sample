### My profile image module for the Common-Tater application ###

* Common-tater is a browser application written in JavaScript. It maintains state and manages access control using a Firebase database. The application source code is built with Browserify and may be served using a simple static file http server. 

### What is this module for? ###

* While working with Common-tater I was tasked with creating a new module that would handle all of the profile image needs within the app. There were multiple areas where the profile image was being used, and as a result, when I came aboard, there was duplicate code throughout the codebase. My new module handles dynamic updates of the image, as well as handling the social link icons and functionality that comes along with it.