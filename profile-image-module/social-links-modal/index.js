module.exports = SocialLinksModal

var hg = require('hyperglue2')
var router = require('uri-router')
var template = require('./template.html')
var TransitionView = require('../../../../../transition-view')
var User = require('../../../../model')

TransitionView.extend(SocialLinksModal)

SocialLinksModal.zIndex = 2

function SocialLinksModal () {
  this.el = hg(template)

  this.render = this.render.bind(this)
  this._onclick = this._onclick.bind(this)
  this._onkeypress = this._onkeypress.bind(this)
  this.save = this.save.bind(this)

  this.el.addEventListener('click', this._onclick)
  this.el.querySelector('[name=facebook-link]').addEventListener('keypress', this._onkeypress)
  this.el.querySelector('[name=instagram-link]').addEventListener('keypress', this._onkeypress)
  this.el.querySelector('[name=twitter-link]').addEventListener('keypress', this._onkeypress)

  this.el.querySelector('#ok').focus()
}

SocialLinksModal.prototype.show = function (r) {
  this.init = this.init || r.init
  this.render()
  if (!User.current) {
    router.push(window.location.pathname + '?action=email#signin')
    return
  } else {
    this.init = true
  }

  TransitionView.prototype.show.call(this, r)

  if (!this.model) {
    this.model = new User({id: User.current.data.id})
    this.model.on('update', this.render)
  }

  this.model.watch()

  if (r.location.query.minimal) {
    hg(this.el, {
      '#options': {_class: {hidden: true}},
      '#cancel': {_text: 'Skip'}
    })
  }
}

SocialLinksModal.prototype.render = function () {
  if (!this.model || !this.model.loaded) {
    return
  }

  hg(this.el, {
    '[name="facebook-link"]': {
      _value: this.model.data.facebook_url
    },
    '[name="instagram-link"]': {
      _value: this.model.data.instagram_url
    },
    '[name="twitter-link"]': {
      _value: this.model.data.twitter_url
    }
  })
}

SocialLinksModal.prototype._validateSocialUrl = function (url, domain) {
  if (url === '') {
    return null
  }
  var urlRegexStr = '^(http|https):..(www\.)' + domain + '\.com\/(.+)?'
  var urlRegex = new RegExp(urlRegexStr)
  var regexMatch = url.match(urlRegex)
  if (!regexMatch) {
    return false
  }
  var completeUrl = 'https://www.' + domain + '.com/' + regexMatch[3]
  return completeUrl
}

SocialLinksModal.prototype.save = function () {
  var self = this

  var facebookLink = this.el.querySelector('[name=facebook-link]').value
  var instagramLink = this.el.querySelector('[name=instagram-link]').value
  var twitterLink = this.el.querySelector('[name=twitter-link]').value

  var allUrlsAreValid = true
  var facebookUrl = this._validateSocialUrl(facebookLink, 'facebook')
  allUrlsAreValid = facebookUrl !== false
  var instagramUrl = this._validateSocialUrl(instagramLink, 'instgram')
  allUrlsAreValid = (instagramUrl !== false) && (allUrlsAreValid)
  var twitterUrl = this._validateSocialUrl(twitterLink, 'twitter')
  allUrlsAreValid = (twitterUrl !== false) && (allUrlsAreValid)

  if (!allUrlsAreValid) {
    hg(this.el, {
     '#error': {
        _text: 'The link you entered is invalid',
        _class: {
          'hidden': false
        }
      }
    })
    return
  }

  this.model.data.facebook_url = facebookUrl
  this.model.data.instagram_url = instagramUrl
  this.model.data.twitter_url = twitterUrl

  this.model.update(function (err) {
    if (err) {
      err.message = 'The link you entered is invalid'

      hg(self.el, {
       '#error': {
          _text: err.message,
          _class: {
            'hidden': false
          }
        }
      })
    } else {
      self.close()
    }
  })
}
// Testing out new error message
SocialLinksModal.prototype._onclick = function (evt, err) {
  if (err) {
    err.message = 'The Link you entered is invalid'
    hg(this.el, {
      '#error': {
        _text: err.message,
        _class: {
          'hidden': false
        }
      }
    })
  }
  if (evt.target.id === 'cancel') {
    this.close()
  } else if (evt.target.id === 'ok') {
    this.save()
  }
}

SocialLinksModal.prototype._onkeypress = function (evt) {
  var key = evt.which || evt.keyCode
  if (key === 13) { // 13 is enter
    evt.preventDefault()
    this.save()
  }
}

SocialLinksModal.prototype.close = function () {
  if (this.init) {
    router.redirect(window.location.pathname)
  } else {
    window.history.back()
  }
}

SocialLinksModal.prototype.hide = function (r, cb) {
  if (this.model) {
    this.model.removeListener('update', this.render)
    this.model.unwatch()
    delete this.model
  }

  TransitionView.prototype.hide.call(this, r, cb)
}
