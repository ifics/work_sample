module.exports = ExtrasView

var hg = require('hyperglue2')
var template = require('./template.html')
var User = require('../../../../model')

function ExtrasView () {
  this.el = hg(template)
  this.render = this.render.bind(this)
}

ExtrasView.prototype.show = function () {
  if (!this.model) {
    this.model = new User({id: this.id})
    this.model.on('update', this.render)
    this.render()
  }

  this.model.watch()
}

ExtrasView.prototype.render = function () {
  if (!this.model || !this.model.loaded || this.model.notFound) {
    return
  }

  hg(this.el, {
    '#protater-badge': {_class: {hidden: !this.model.data.pro_badge }}
  })
}

ExtrasView.prototype.hide = function (cb) {
  hg(this.el, {'#protater-badge': {_class: {hidden: true}}})

  if (this.model) {
    this.model.removeListener('update', this.render)
    this.model.unwatch()
    delete this.model
  }
}
