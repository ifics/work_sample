module.exports = ProfileImageView

var hg = require('hyperglue2')
var template = require('./template.html')
var dragAndDropFiles = require('drag-and-drop-files')
var User = require('../../../model')
var ImageModel = require('../../../../images/model')
var ExtrasView = require('./extras')
var SocialLinksView = require('./social-links')
var SocialLinksModal = require('./social-links-modal')

function ProfileImageView (enableUpload) {
  var self = this
  this.el = hg(template)

  this._enableUpload = enableUpload
  this._loadImageFromFile = this._loadImageFromFile.bind(this)
  this._refresh = this._refresh.bind(this)
  this.render = this.render.bind(this)

  this.extrasView = new ExtrasView()
  this._hasLoadedChildViews = false
  this.el.querySelector('#extras-outlet').appendChild(this.extrasView.el)
  this.socialLinksView = new SocialLinksView()
  this.el.querySelector('#social-link-outlet').appendChild(this.socialLinksView.el)
  this.socialLinksModal = new SocialLinksModal()

  var fileUploadEl = this.el.querySelector('#file-upload')
  fileUploadEl.addEventListener('change', function (evt) {
    // sometimes this gets triggered when a file is canceled,
    // so make sure it's defined
    if (fileUploadEl.files[0]) {
      self._loadImageFromFile(fileUploadEl.files[0])
    }
  })
}

ProfileImageView.prototype.show = function () {
  var self = this

  var drop = this.el.querySelector('#file-upload')
  this.fileToUpload = null

  // set up the drag-and-drop funtionality
  dragAndDropFiles(drop, function (files) {
    // why does this get called twice? it forces me to
    // do this check to see if it's already been done
    if (!self.fileToUpload) {
      self.fileToUpload = files[0]
      self._loadImageFromFile(self.fileToUpload)
    }
  })

  this.render()
}

ProfileImageView.prototype.render = function () {
  var user = this.user && this.user.loaded ? this.user : {}

  if (user.data) {
    if (!this._hasLoadedChildViews) {
      this._hasLoadedChildViews = true
      this.extrasView.id = user.data.id
      this.extrasView.show()
      this.socialLinksView.id = user.data.id
      this.socialLinksView.show()
      this.socialLinksModal.id = user.data.id
    }

    if (this.model && this.model.downloadedImage) {
      this.imageUrl = 'data:image/jpeg;base64,' + this.model.downloadedImage.toString('base64')
      hg(this.el, {
        '#result': ''
      })
    } else if (user.data.image_id) {
      // if this image model hasn't been loaded yet, load it
      if (!this.model) {
        this.model = new ImageModel({ id: user.data.image_id })
        this.model.on('update', this.render)
        this.model.watch()
      } else {
        this.imageUrl = this.model.data.url
      }
    }

    hg(this.el, {
      '#file-upload': {
        _class: {
          hidden: !this._enableUpload || (!User.current || (User.current.data.id !== user.data.id))
        }
      }
    })
  }

  if (!this.imageUrl) {
    this.imageUrl = '/images/missing-profile.png'
  }

  this._refresh()
}

// iOS reloads the image even if the url has not
// changed so we update the profile image manually
ProfileImageView.prototype._refresh = function () {
  var image = this.el.querySelector('#preview')
  if (!image) {
    return
  }

  var style = 'background-image: url(' + this.imageUrl + ')'
  if (image.getAttribute('style') !== style) {
    image.setAttribute('style', style)
  }
}

ProfileImageView.prototype._loadImageFromFile = function (file) {
  hg(this.el, {
    '#result': 'Loading...'
  })
  this._shouldRefresh = true
  this.model = new ImageModel({
    user_id: User.current.data.id
  }, file, process.env.MAX_PROFILE_IMAGE_SIZE)
  this.model.on('update', this.render)
}

ProfileImageView.prototype.hide = function () {
  if (this.model) {
    this.model.removeListener('update', this.render)
    this.model.unwatch()
    delete this.model
  }
  this.extrasView.hide()
  this.socialLinksView.hide()

  delete this.imageUrl
  delete this._hasLoadedChildViews
  delete this.user
}
