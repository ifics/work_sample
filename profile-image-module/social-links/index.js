module.exports = SocialLinksView

var hg = require('hyperglue2')
var template = require('./template.html')
var User = require('../../../../model')

function SocialLinksView () {
  this.el = hg(template)

  this.render = this.render.bind(this)
  this._onclick = this._onclick.bind(this)

  this.el.addEventListener('click', this._onclick)
}

SocialLinksView.prototype.show = function () {
  if (!this.model) {
    this.model = new User({id: this.id})
    this.model.on('update', this.render)
    this.render()
  }

  this.model.watch()
}

SocialLinksView.prototype._onclick = function (evt) {
  if (this.el.contains(evt.target)) {
    window.localStorage.userClickedSocialLink = true
  }
}

SocialLinksView.prototype.render = function () {
  if (!this.model || !this.model.loaded || this.model.notFound) {
    return
  }

  var isSelf = User.current && User.current.data.id === this.model.data.id
  var modalLink = '#social-links'

  if (window.localStorage.userClickedSocialLink === undefined) {
    hg(this.el, {
      '#facebook-font': {
        _class: {
          'shake': !this.model.data.facebook_url,
          'animated': !this.model.data.facebook_url
        }
      },
      '#instagram-font': {
        _class: {
          'shake': !this.model.data.instagram_url,
          'animated': !this.model.data.instagram_url
        }
      },
      '#twitter-font': {
        _class: {
          'shake': !this.model.data.twitter_url,
          'animated': !this.model.data.twitter_url
        }
      }
    })
  }

  hg(this.el, {
    '#facebook-font': {
      _class: {
        'facebook-button-color': this.model.data.facebook_url
      }
    },
    '#instagram-font': {
      _class: {
        'instagram-button-color': this.model.data.instagram_url
      }
    },
    '#twitter-font': {
      _class: {
        'twitter-button-color': this.model.data.twitter_url
      }
    }
  })

  if (!isSelf) {
    hg(this.el, {
      '#facebook > a': {
        _attr: {
          'href': this.model.data.facebook_url
        }
      },
      '#instagram > a': {
        _attr: {
          'href': this.model.data.instagram_url
        }
      },
      '#twitter > a': {
        _attr: {
          'href': this.model.data.twitter_url
        }
      },
      '#facebook': {
        _class: {
          'hidden': !this.model.data.facebook_url
        }
      },
      '#instagram': {
        _class: {
          'hidden': !this.model.data.instagram_url
        }
      },
      '#twitter': {
        _class: {
          'hidden': !this.model.data.twitter_url
        }
      }
    })
  } else {
    hg(this.el, {
        '#facebook > a': {
          _attr: {
            'href': modalLink
          }
        },
        '#instagram > a': {
          _attr: {
            'href': modalLink
          }
        },
        '#twitter > a': {
          _attr: {
            'href': modalLink
          }
        }
      })
  }
}

SocialLinksView.prototype.hide = function (cb) {
  if (this.model) {
    this.model.removeListener('update', this.render)
    this.model.unwatch()
    delete this.model
  }
}
